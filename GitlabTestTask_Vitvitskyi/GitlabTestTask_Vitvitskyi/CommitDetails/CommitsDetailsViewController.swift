//
//  CommitsDetailsViewController.swift
//  GitlabTestTask_Vitvitskyi
//
//  Created by Mac on 22.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import UIKit

final class CommitsDetailsViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var authorEmailLabel: UILabel!
    @IBOutlet private weak var authorNameLabel: UILabel!
    @IBOutlet private weak var commitedDateLabel: UILabel!
    @IBOutlet private weak var commitedEmailLabel: UILabel!
    @IBOutlet private weak var createdAtLabel: UILabel!
    @IBOutlet private weak var idLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var shortIdLabel: UILabel!
    
    // MARK: - Methods
    
    var commit: Commit!
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
    }
    
    // MARK: - Methods
    
    private func setUpUI() {
        let authorEmail = commit.authorEmail ?? "-"
        authorEmailLabel.text = "Author email:  \(authorEmail)"
        let commiterName = commit.committerName ?? "-"
        authorNameLabel.text = "Committer name:  \(commiterName)"
        let commitedDate = commit.committedDate ?? "-"
        commitedDateLabel.text = "Commited Date:  \(commitedDate)"
        let commitedEmai = commit.committerEmail ?? "-"
        commitedEmailLabel.text = "Commited email:  \(commitedEmai)"
        let createdAt = commit.createdAt ?? "-"
        createdAtLabel.text = "Created at:  \(createdAt)"
        let id = commit.id ?? "-"
        idLabel.text = "Id:  \(id)"
        let message = commit.message ?? "-"
        messageLabel.text = "Message:  \(message)"
        let shortId = commit.shortId ?? "-"
        shortIdLabel.text = "Short id:  \(shortId)"
    }
}



