//
//  UIViewController+Alerts.swift
//  GitlabTestTask_Vitvitskyi
//
//  Created by Mac on 22.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(_ title: String?,
                   message: String?,
                   comletionHandler: ((UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: comletionHandler)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
