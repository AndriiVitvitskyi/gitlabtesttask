//
//  UIStoryboard+RetrievingVCs.swift
//  GitlabTestTask_Vitvitskyi
//
//  Created by Mac on 22.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import UIKit

extension UIStoryboard {
    private enum Name {
        static let main = UIStoryboard(name: "Main", bundle: nil)
        static let commitDetails = UIStoryboard(name: "CommitsDetails", bundle: nil)
    }
    
    enum Main {
        static var main: CommitsViewController {
            return Name.main.instantiateViewController(withType: CommitsViewController.self)
        }
    }
    
    enum CommitDetails {
        static var recoveryPasswordInitial: CommitsDetailsViewController {
            return Name.commitDetails.instantiateViewController(withType: CommitsDetailsViewController.self)
        }
    }
}

