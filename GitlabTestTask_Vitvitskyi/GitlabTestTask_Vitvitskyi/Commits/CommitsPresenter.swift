//
//  CommitsPresenter.swift
//  GitlabTestTask_Vitvitskyi
//
//  Created by Mac on 22.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

protocol CommitsPresenterInterface: class {
    var commits: [Commit] { get set }
    var delegate: CommitsPresenterDelegate? { get set }
    func getCommits()
}

protocol CommitsPresenterDelegate: class {
    func showErrorMessage(_ message: String)
    func reloadCommitTableView()
}

final class CommitsPresenter: CommitsPresenterInterface {
    
    // MARK: - Properties
    
    weak var delegate: CommitsPresenterDelegate?
    private let gitLabService = GitLabService()
    var commits: [Commit] = []
    
    // MARK: - Methods
    
    func getCommits() {
        DispatchQueue.global(qos: .utility).async {
            self.gitLabService.gitLabExample(callback: Callback<[Commit]>(
                onSuccess: { [weak self] commits in
                    _ = commits.map ({ [weak self] in
                        self?.commits.append($0)
                        self?.delegate?.reloadCommitTableView()
                    })
                }, onFailure: { [weak self] error in
                    self?.delegate?.showErrorMessage(error.description)
                }
            ))
        }
    }
    
}


