//
//  ViewController.swift
//  GitlabTestTask_Vitvitskyi
//
//  Created by Mac on 22.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import UIKit

final class CommitsViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet private weak var commitsTableView: UITableView!
    
    // MARK: - Properties
    
    private var commitsPresenter: CommitsPresenterInterface?
    
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commitsPresenter = CommitsPresenter()
        commitsPresenter?.delegate = self
        setUpTableView()
        commitsPresenter?.getCommits()
    }
    
    // MARK: - Methods
    
    private func setUpTableView() {
        commitsTableView.estimatedRowHeight = 200.0
        commitsTableView.rowHeight = UITableView.automaticDimension
        commitsTableView.separatorStyle = .none
        commitsTableView.delegate = self
        commitsTableView.dataSource = self
    }

}

// MARK: - UITableViewDelegate
extension CommitsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commitsPresenter?.commits.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CommitTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.commitId = commitsPresenter?.commits[indexPath.row].id
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        let commitDetailsVC = UIStoryboard.CommitDetails.recoveryPasswordInitial
        commitDetailsVC.commit = commitsPresenter?.commits[indexPath.row]
        navigationController?.pushViewController(commitDetailsVC, animated: true)
    }
}

// MARK: - CommitsPresenterDelegate
extension CommitsViewController: CommitsPresenterDelegate {
    
    func showErrorMessage(_ message: String) {
        DispatchQueue.main.async {
            self.showAlert("Error", message: message)
        }
    }
    
    func reloadCommitTableView() {
        DispatchQueue.main.async {
            self.commitsTableView.reloadData()
        }
    }
}
