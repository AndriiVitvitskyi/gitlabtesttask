//
//  CommitTableViewCell.swift
//  GitlabTestTask_Vitvitskyi
//
//  Created by Mac on 22.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import UIKit


final class CommitTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var commitIdLabel: UILabel!
    
    var  commitId: String? {
        didSet {
            commitIdLabel.text = commitId ?? ""
        }
    }

}

extension CommitTableViewCell: ReusableView, NibLoadableView {
    
}
