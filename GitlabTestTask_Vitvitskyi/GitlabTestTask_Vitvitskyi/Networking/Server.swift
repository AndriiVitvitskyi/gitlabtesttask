//
//  Server.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

protocol ServerInterface {
    var scheme: ServerScheme { get }
    var host: String { get }
}

enum ServerScheme: String {
    case http
    case https
}

struct Server: ServerInterface {
    var scheme: ServerScheme
    var host: String
    
    private init(scheme: ServerScheme, host: String) {
        self.scheme = scheme
        self.host = host
    }
    
    private init (_ server: ServerInterface) {
        self.init(scheme: server.scheme,
                  host: server.host)
    }
}


// Client

// Helpfull extension
extension Server {
    static var gitLabServer: Server {
        return .init(GitLabServer())
    }
}

// Separate struct for each Server

struct GitLabServer: ServerInterface {
    var scheme: ServerScheme {
        return .https
    }
    
    var host: String {
        return "gitlab.com"
    }
}
