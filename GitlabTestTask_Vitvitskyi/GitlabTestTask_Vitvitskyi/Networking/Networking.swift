//
//  Networking.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

struct EmptyType: Encodable { }
struct EmptyResult: Decodable { }

protocol RequestPerformable {
    var session: URLSession { get }
    var decoder: JSONDecoder { get }
    func gitLabExample(callback: Callback<[Commit]>)
}

extension RequestPerformable {
    
    // MARK: - Stuff
    var session: URLSession {
        return URLSession.shared
    }
    
    var decoder: JSONDecoder {
        return JSONDecoder.snakeCaseDecoder()
    }
    
    // MARK: - DataTask
    func performDataTask<ParsedType: Decodable>(with request: RequestCreatable,
                                                logsEnable: Bool = false, callBack: Callback<ParsedType>) {
        
        let callback = Callback<Any>()
        
        let dataTask = session.dataTask(with: request.asURLRequest()) { (data, response, error) in
            
            /// Handle Error
            if let networkingError = self.handleError(error) {
                callBack.onFailure(networkingError)
            }
            
            /// Check data and response
            guard let data = data,
                let response = response as? HTTPURLResponse else {
                    callBack.onFailure(NetworkingError.badData)
                    return
            }
            
            /// LOGs
            if logsEnable {
                self.printLogs(with: request,
                               response: response,
                               parsedType: ParsedType.self,
                               data: data)
            }

            guard response.isStatusCodeInOkRange else {
                if let responseError = self.checkErrorInResponse(data: data) {
                    callBack.onFailure(responseError)
                }
                print("Smth goes wrong")
                callBack.onFailure(NetworkingError.defaultError)
                return
            }
            
            /// Check if needed only true or false (if response have an empty JSON, and we need only checking status code)
            guard "\(ParsedType.self)" != "EmptyResult" else {
                
                let emptyResult = self.getEmptyResult(parsedType: ParsedType.self)
                callBack.onSuccess(emptyResult)
                return
            }
            
            do {
                let parsedData = try self.decoder.decode(ParsedType.self, from: data)
                callBack.onSuccess(parsedData)
            } catch let catchError {
                callBack.onFailure(NetworkingError.defaultError)
            }
        }
        dataTask.resume()
    }
}

// MARK: - Helpfull methods
extension RequestPerformable {
    
    // MARK: - Error handling
    private func handleError(_ error: Error?) -> NetworkingError? {
        guard let error = error else { return nil }
        // TODO: Handle error
        // -1001 timeout
        // -1009 inet connection
        print("\nNetworking ERROR: ", error, "\n")
        return NetworkingError.defaultError
    }
    
    private func checkErrorInResponse(data: Data) -> NetworkingError? {
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
            if let error = json["error"] as? String {
                return NetworkingError.responseError(error)
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
        
        return nil
    }
    
    // MARK: - LOGs
    private func printLogs<ParsedType: Decodable>(with request: RequestCreatable,
                                                  response: HTTPURLResponse,
                                                  parsedType: ParsedType.Type,
                                                  data: Data) {
        print("\nFinal URL: \(request.endpoint.asURL())")
        print("\nResponse code: \(response.statusCode) (\(ParsedType.self))" )
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            print("RESPONSE JSON: ", json)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Empty result
    private func getEmptyResult<ParsedType: Decodable>(parsedType: ParsedType.Type) -> ParsedType {
        
        let emptyEncodedObj = EmptyType()
        let data = try! emptyEncodedObj.myData()
        
        return try! self.decoder.decode(ParsedType.self, from: data)
    }
    
}
