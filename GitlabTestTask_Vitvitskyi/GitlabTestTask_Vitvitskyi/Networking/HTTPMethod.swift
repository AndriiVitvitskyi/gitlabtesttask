//
//  HTTPMethod.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

enum HTTPMethod : String {
    case GET
    case POST
    case PUT
    case DELETE
}
