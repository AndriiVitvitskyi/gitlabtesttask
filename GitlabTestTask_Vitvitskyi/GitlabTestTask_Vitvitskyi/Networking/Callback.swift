//
//  Callback.swift
//  GitlabTestTask_Vitvitskyi
//
//  Created by Mac on 23.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

open class Callback<T> {
    
    open var onSuccess: (T) -> ()
    open var onFailure: (NetworkingError) -> ()
    
    public init() {
        onSuccess = { result in }
        onFailure = { error in }
    }
    
    public init(onSuccess: @escaping (T) -> (), onFailure: @escaping (NetworkingError) -> ()) {
        self.onSuccess = onSuccess
        self.onFailure = onFailure
    }
    
}
