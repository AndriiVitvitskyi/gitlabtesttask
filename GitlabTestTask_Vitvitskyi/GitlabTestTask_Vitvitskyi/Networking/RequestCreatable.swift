//
//  RequestCreatable.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

protocol RequestCreatable {
    var endpoint: Endpoint { get set }
    var body: Encodable? { get set }
    
    func addStandartHeadersFor(request: inout URLRequest)
    func addParametersFor(request: inout URLRequest, parameters: Encodable) throws
    
    func asURLRequest() -> URLRequest
}

extension RequestCreatable {
    
    func addStandartHeadersFor(request: inout URLRequest) {
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    }
    
    func addAuthHeaderFor(request: inout URLRequest) {
        // Get token from keychailn
    }
    
    func addParametersFor(request: inout URLRequest, parameters: Encodable) throws {
        do {
            let data = try parameters.myData()
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            print("BODY JSON: ", json)
            request.httpBody = data
        } catch {
            
            throw NetworkingError.defaultError
        }
    }
    
    func asURLRequest() -> URLRequest {
        
        // Make a request and adding headers with token etc.
        let destinationUrl = endpoint.asURL()
        var urlRequest = URLRequest(url: destinationUrl)
        
        urlRequest.httpMethod = endpoint.method.rawValue
        addStandartHeadersFor(request: &urlRequest)
        
        if endpoint.isAuthTokenRequired {
            addAuthHeaderFor(request: &urlRequest)
        }
        
        if let body = body {
            do {
                try addParametersFor(request: &urlRequest, parameters: body)
            } catch {
                
                print("Error in asURLRequest: ", error.localizedDescription)
                assert(false, "Can't add parameters for request")
            }
        }
        
        // Additional request settings
        urlRequest.timeoutInterval = 10
        
        return urlRequest
    }
    
}
