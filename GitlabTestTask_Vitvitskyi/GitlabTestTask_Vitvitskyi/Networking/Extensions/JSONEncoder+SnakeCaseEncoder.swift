//
//  JSONEncoder+SnakeCaseEncoder.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

extension JSONEncoder {
    static func snakeCaseEncoder() -> JSONEncoder {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        
        return encoder
    }
}
