//
//  Error+NSErrorFeatures.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

extension Error {
    var code: Int {
        return (self as NSError).code
    }
    
    var domain: String {
        return (self as NSError).domain
    }
}
