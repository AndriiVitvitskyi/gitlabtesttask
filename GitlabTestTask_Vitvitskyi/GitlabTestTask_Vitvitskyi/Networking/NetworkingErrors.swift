//
//  NetworkingErrors.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

public enum NetworkingError: Error {
    case defaultError
    case badData
    case responseError(String)
    
    var description: String {
        switch self {
        case .defaultError:
            return "Standart error"
        case .badData:
            return "No data or response"
        case .responseError(let errorText):
            return errorText
        }
        
    }
}
