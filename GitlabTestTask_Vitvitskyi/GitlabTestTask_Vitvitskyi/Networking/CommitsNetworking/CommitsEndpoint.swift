//
//  CommitsEndpoint.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

enum GitLabEndpoint: Endpoint {
    case getCommits
    
    var method: HTTPMethod {
        switch  self {
        case .getCommits:
            return .GET
        }
    }
    
    var isAuthTokenRequired: Bool { return false }
    
    var server: Server {
        return .gitLabServer
    }
    
    var path: String {
        switch self {
        case .getCommits:
            return "/api/v4/projects/11969026/repository/commits"
        }
    }
    
    var queries: Encodable? {
        return nil
    }
    
    
}
