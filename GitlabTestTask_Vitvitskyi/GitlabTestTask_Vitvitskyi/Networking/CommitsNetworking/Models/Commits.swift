//
//  Commits.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

struct Commit: Decodable {
    let authorEmail: String?
    let authorName: String?
    let committerName: String?
    let committedDate: String?
    let committerEmail: String?
    let createdAt: String?
    let id: String?
    let message: String?
    let shortId: String?
    let title: String?
}




