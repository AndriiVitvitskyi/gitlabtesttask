//
//  CommitsService.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

struct GitLabService: RequestPerformable {
    func gitLabExample(callback: Callback<[Commit]>) {
        let endpoint = GitLabEndpoint.getCommits
        let request = CustomRequest(endpoint: endpoint)
        performDataTask(with: request, callBack: Callback<[Commit]>(
            onSuccess: { commits in
               callback.onSuccess(commits)
        }, onFailure: { error in
            callback.onFailure(error)
        }))
    }
}
