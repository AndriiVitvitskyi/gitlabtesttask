//
//  File.swift
//  GitLabTestTask
//
//  Created by Mac on 21.04.19.
//  Copyright © 2019 Vitvitskyi. All rights reserved.
//

import Foundation

typealias Endpoint = RequestContructable & URLConstructable

struct CustomRequest: RequestCreatable {
    
    var endpoint: Endpoint
    var body: Encodable?
    
    init(endpoint: Endpoint, body: Encodable? = nil) {
        self.endpoint = endpoint
        self.body = body
    }
}
